mod universe;

fn main() {
    let universe = universe::Universe([0; 256]);
    println!("Hello, world! {}", universe);
}
